#!/bin/sh

for BUILD_DIR in `find . -type d -name build`
do
  PDF_DIR=documents/`dirname $BUILD_DIR`
  mkdir -p $PDF_DIR
  cp $BUILD_DIR/*.pdf $PDF_DIR
done
