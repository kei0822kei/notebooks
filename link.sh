#!/bin/sh

for DIR in `find machine_learning math -mindepth 1 -maxdepth 1 -type d`
do
  ln -nsf `pwd`/common $DIR/common
done
